# DWPAssesmentTest


Assessment Running on Windows

If Eclipse is not installed, you can install it from https://www.eclipse.org/downloads/ 

This project was tested on a Windows machine with Eclipse IDE

1.	Open Eclipse
2.	Browse to the folder where the project was clone to then click the Launch button
3.	If Required update all the NuGet packages
4.	Right click on console and click ‘Run As’ then “TestNG Test” to run test
5.	Test should start running and result will be displayed in the console window.



