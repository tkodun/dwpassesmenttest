package DWP_Request;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.hamcrest.Matcher;

import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class getCity {
	@Test
	public void getCityName()
	{
		given()
		    .contentType("application/json")
		.when()
		    .get("http://bpdts-test-app-v2.herokuapp.com/city/Manchester/users")
		.then()
		 .assertThat()
	     .statusCode(200);
	}
	
	@Test
	public void getInstructionResponse()
	{
		given()
	    .contentType("application/json")
	.when()
	    .get("http://bpdts-test-app-v2.herokuapp.com/instructions")
	.then()
	     .assertThat()
	     .statusCode(200)
	     .body("todo", containsString("Create a short automated test for this API.") );
	}
	
	
	  @Test 
	  public void getUsers() 
	  { 
	  given() 
	      .contentType("application/json")
	  .when() 
	      .get("http://bpdts-test-app-v2.herokuapp.com/users") 
	  .then()
	      .assertThat() 
	      .statusCode(200) 
	      .body("size()", is(1000)) 
	      .body("id", notNullValue()) 
	      .body("first_name", notNullValue()) 
	      .body("last_name", notNullValue()) 
	      .body("email", notNullValue()) 
	      .body("ip_address", notNullValue()) 
	      .body("latitude", notNullValue()) 
	      .body("longitude", notNullValue()); 
	  }
	 
	
	  @Test 
	  public void getSingleUser4() 
	  { 
	  given() 
		  .contentType("application/json")
	  .when() 
	      .get("http://bpdts-test-app-v2.herokuapp.com/user/4") 
	  .then()
	      .assertThat() 
	      .statusCode(200) 
	      .body("id", equalTo(4))
	      .body("first_name", equalTo("Sidnee")) 
	      .body("last_name", equalTo("Silwood"))
	      .body("email", equalTo("ssilwood3@gizmodo.com")) 
	      .body("ip_address", equalTo("77.55.231.220"))
	      .body("latitude", equalTo(-26.94087f)) 
	      .body("longitude", equalTo(29.24905f))
	      .body("city", equalTo("Standerton")); 
	  }
	  
	 
	  @Test 
	  public void getSingleUser999() 
	  { 
	  given() 
		  .contentType("application/json")
	  .when() 
	      .get("http://bpdts-test-app-v2.herokuapp.com/user/999") 
	  .then()
	      .assertThat() 
	      .statusCode(200) 
	      .body("id", equalTo(999))
	      .body("first_name", equalTo("Celisse")) 
	      .body("last_name", equalTo("Haug"))
	      .body("email", equalTo("chaugrq@stumbleupon.com")) 
	      .body("ip_address", equalTo("18.224.170.176"))
	      .body("latitude", equalTo(59.2830988f)) 
	      .body("longitude", equalTo(18.033347f))
	      .body("city", equalTo("�lvsj�")); 
	  }	 
}
